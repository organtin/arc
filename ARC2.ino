/*

   ARC: Appliances Remote Control
   Copyright (C) 2014 by Giovanni.Organtini@roma1.infn.it
  
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Version history:
   ====================================================================
   1.1: better memory managemen (using F macro)
        added SMS support 
   1.0: first working prototype

   Defined SMS commands
   ====================================================================
   USER: returns the current user of the system and the timestamp at 
         which he/she started the system

*/

#include <SoftwareSerial.h>
#include <String.h>

/*
   Configuration starting
*/

#define NPHONES 2

String phones[NPHONES] = { "39XXXXXXXXXX", "39XXXXXXXXXX" };
String aliases[NPHONES] = { "Giovanni", "Federica" };

#define NODEBUG 0
#define    FULL 9

#define LED1  2
#define LED2  3
#define RELAY 5

SoftwareSerial SIM900(7, 8);
int debugLevel = NODEBUG;
boolean _ison;
String  _starttime;
String  _owner;

void debug(const String &msg) {
  //
  // show debug message
  //
  return debug(msg, true);
}

void debug(const String &msg, boolean nl) {
  //
  // show debug messages
  //
  if (debugLevel > NODEBUG) {
    Serial.print(msg);
    if (nl) {
      Serial.println();
    }
  }
}

String waitForMessage(const String &terminator) {
  //
  // read chars from SIM900 until it finds terminator
  //
  boolean loop = true;
  String ret = "";
  debug("Wait for " + terminator);
  while (loop) {
    if (SIM900.available()) {
      char c = SIM900.read();
      if (c == 0) {
        ret += "\\0";
      } else if (c == 13) {
        ret += "\\r";
      } else if (c == 10) {
        ret += "\\n";
      } else if ((c < 31) || (c> 126)) {
        // non printable characters substituted by a fullstop
        ret += ".";
      } else {
        ret += c;
      }
      if (debugLevel > NODEBUG) {
        if ((c < 32) || (c> 126)) {
          c = '.';
        }
        Serial.print(c);
      }
      if (ret.indexOf("POWER DOWN") >= 0) {
        SIM900power();
        ret = "";
      }
      if (ret.indexOf("+CMTI: \"SM\",") >= 0) {
        // an SMS arrived in the meantime
        incomingsms(ret);
        ret = "";
      }
      if (ret.indexOf(terminator) >= 0) {
        loop = false;
      }
    }
  }
  if (debugLevel > NODEBUG) {
    Serial.println();
  }
  debug("Returning " + ret);
  return ret;
}

String alias(const String &phone) {
  int i = 0;
  String ret = "Unknown";
  while (i < NPHONES) {
    if (phone.equals(phones[i])) {
      ret = aliases[i];
      i = NPHONES;
    }
    i++;
  }
  return ret;
}

void incomingsms(const String &msg) {
  //
  // incoming SMS
  //
  debug("\nSMS Detected");
  // read the SMS
  String sms = atcommand("AT+CMGR=1");
  String caller = sms.substring(sms.indexOf("UNREAD") + 10);
  caller = caller.substring(0, caller.indexOf("\""));
  sms = sms.substring(sms.lastIndexOf("\"") + 5);
  sms = sms.substring(0, sms.length() - 10);
  sms.toUpperCase();
  debug("Caller: " + caller);
  debug("Text  : " + sms);
  if (phoneIsValid(caller)) {
    if (sms.indexOf("USER") >= 0) {
      sendsms(alias(_owner) + ": " + _starttime, caller);
    }
  }
  // delete all of them for the moment
  atcommand(F("AT+CMGD=1,4"));
}

int freeRam () {
  // stolen from adafruit blog
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void SIM900start() {
  //
  // configure the modem
  //
  pinMode(9, OUTPUT);
  SIM900power();
  SIM900.begin(19200);
  waitForMessage("Call Ready");
  if (debugLevel > NODEBUG) {
    Serial.print("Free ram: ");
    Serial.println(freeRam());
  }
  atcommand(F("ATZ0")); // reset to factory default
  atcommand(F("ATS7=10")); // wait 10 seconds before giving up
  atcommand(F("AT+CMGF=1")); // text mode for SMS
  atcommand(F("AT+CLIP=1")); // enable identification of the caller
  atcommand(F("AT+CLTS=1")); // get the time at startup
  atcommand(F("ATQ0"));  // do not suppress result code
  atcommand(F("AT+COLP=0")); // immediately return after dialing
  atcommand(F("ATV1")); // full code returning
  atcommand(F("AT+CMGD=1,4")); // delete all untreated SMS
  atcommand(F("AT+CENG=3"));
  waitForMessage("DST: 1"); // the last command returns local time + DST
  return;
}

String atcommand(const __FlashStringHelper *pData) {
  String cmd = "";
  char *s = (char *)pData;
  char c;
  while ((c = pgm_read_byte(s++)) != 0) {
    cmd += c;
  }
  return atcommand(cmd);
}

String atcommand(const String &cmd) {
  return atcommand(cmd, "OK");
}

String atcommand(const String &cmd, const String &response) {
  //
  // execute an AT command
  //
  debug("Executing " + cmd);
  SIM900.print(cmd + "\r");
  delay(100);
  String ret = waitForMessage(response);
  return ret;
}

void SIM900power() {
  //
  // software equivalent of pressing the GSM shield "power" button
  //
  debug("Powering up...");
  digitalWrite(9, LOW);
  delay(1000);
  digitalWrite(9, HIGH);
  delay(2000);
  return;
}

void call() {
  //
  // place a call (but do not wait for answer)
  //
  atcommand("ATD+" + phones[0] + ";");
  waitForMessage("NO ANSWER");
}

void sendsms(const String &message, const String &recipient) {
  //
  // send an sms to recipient
  //
  atcommand("AT+CMGS=\"+" + recipient + "\"", "> ");
  atcommand(message + "\r" + (char)26, "OK");
  return;  
}

void busy(const String &caller) {
  //
  // notify the caller that the system is busy
  //
  String sms = "System busy since " + _starttime + ". Current owner is " + alias(_owner);
  debug("BUSY: " + sms);
  sendsms(sms, caller);
  return;
}

void off(const String &caller) {
  //
  // switch off
  //
  if (caller.equals(_owner)) {
    // only the owner can switch off an equipment
    _ison = false;
    _owner = "none";
    debug("Owner reset");
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, LOW);
    digitalWrite(RELAY, HIGH);
  } else {
    busy(caller);
  }
  return;
}

void on(const String &caller) {
  //
  // switch on
  //
  _ison = true;
  _starttime = gettime();
  _owner = caller;
  debug("Owner set to " + _owner);
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, HIGH);
  digitalWrite(RELAY, LOW);
  return;
}

String gettime() {
  //
  // get current time
  //
  String ret = atcommand(F("AT+CCLK?"));
  int i = ret.indexOf("+CCLK: \"") + 8;
  ret = ret.substring(i);
  i = ret.indexOf("\"");
  ret = ret.substring(0, i);
  debug("Current time: " + ret);
  return ret;
}

boolean phoneIsValid(const String &phone) {
  //
  // verify if the caller is authorized
  //
  boolean authorized = false;
  int i = 0;
  while (i < NPHONES) {
    if (phones[i].equals(phone)) {
      authorized = true;
      i = NPHONES;
    }
    i++;
  } 
  return authorized;
}

void ringing(String msg) {
  //
  // someone called: the expected msg is something like
  // ....+CLIP: "+39XXXXXXXXXX",145,"",,"YYYYY",0....RING
  //
  if (msg.indexOf("+CLIP") < 0) {
    msg = waitForMessage("RING");
  }
  // once got the number hang up
  atcommand("ATH");
  int i = msg.indexOf("\"+") + 2;
  int j = msg.indexOf("\",");
  String caller = "";
  if ((i >= 0) && (j > i)) {
    caller = msg.substring(i, j);
  }
  if (phoneIsValid(caller)) {
    flip(caller);
  }
  debug("Ringing from " + caller);
  return;
}

void flip(const String &caller) {
  //
  // flip between on and off
  //
  if (_ison) {
    off(caller);
  } else {
    on(caller);
  }
  return;
}

void setup() {
  //
  // Arduino setup
  //
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(RELAY, OUTPUT);
  // keep LEDs off and RELAY open
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  digitalWrite(RELAY, HIGH);  
  if (debugLevel > NODEBUG) {
    Serial.begin(19200);
    Serial.println(F("\nThis is ARC ====================================="));
    Serial.println(F("Copyright (C) by Giovanni.Organtini@roma1.infn.it"));
  }
  SIM900start();
  _owner = "none";
  off("none");
  call();
}

void loop() {
  //
  // Arduino loop
  //
  String ret = waitForMessage("RING");
  ringing(ret);
}
