ARC: Appliance Remote Control
Copyright (C) 2014 by Giovanni.Organtini@roma1.infn.it
==========================================================================

ARC is used to remotely control appliances connected to it. It is
composed of an Arduino UNO coupled to a SIM900 GSM/GPRS shield and
a Relay Module. It is written for a SeedStudio shield, but it should
work with any GSM/GPRS shield, provided you reassign the pins properly.

Deploying
=========
Edit ARC2.ino and change the value of NPHONES to be the number of phones
authorized to use ARC. Then, change the content of the arrays phones and
aliases. The first should contain the list of authorized phone numbers
with international prefix, but without the +. The aliases are free format
strings used to identify the corresponding phones.

Usage
=====
After initialization, a voice call is made to the firts authorized phone in
the phones array. That call ends soon, just to notify the user that the
system is ready. 

If you call ARC from any authorized phone, it activates the relays, giving
power to the appliance connected to it. A second call deactivates the relays.
If a second call comes from another authorized phone, the connection
is refused and an SMS is sent to that phone informing the user who is using
ARC (it uses aliases) and since when. Then, only the activator can deactivate
ARC.




